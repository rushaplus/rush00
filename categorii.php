<!DOCTYPE php>
<html>
<?php
include('header.php');
require_once('dao/categoryDAO.php');
require_once('dao/productDAO.php');
?>
<body>
  <div class="container">
      <?php
      $category = $_GET["cat"];
      $prodList = readFromCategory($category);
      while ($prod = mysqli_fetch_array($prodList))
      {
        echo '<div class="item">
          <img src=""/>
          <h3><a href="produs.php?prod='.$prod['name'].'">'.$prod["name"].'</h3>
          <span class="pret">'.$prod['price'].'
        </div>';
      }
      ?>
  </div>
</body>
</html>
