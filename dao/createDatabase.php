<?php
  function createDb($conn) {
    $dbQuery = "DROP DATABASE rush00";
    mysqli_query($conn, $dbQuery);

    $dbQuery = "Create DATABASE if not exists rush00";
    if (mysqli_query($conn, $dbQuery)) {
      echo "da";
    } else {
      echo "nu";
    }
  }
  function createTables($conn) {
    $sql = " Create TABLE IF NOT EXISTS user(
					id int auto_increment not null PRIMARY KEY,
          name varchar(50) unique not null,
          password varchar(50) not null,
          role int not null);";
    if (mysqli_query($conn, $sql)) {
      echo "da";
      } else {
      echo "nu";
    };
      $sql = "Create TABLE IF NOT EXISTS category (
					id int auto_increment not null PRIMARY KEY,
          name varchar(50) not null);";
          if (mysqli_query($conn, $sql)) {
            echo "da";
          } else {
            echo "nu";
          }
    $sql = "
      Create TABLE IF NOT EXISTS product (
          id int auto_increment not null PRIMARY KEY,
          name varchar(50) not null,
          price float not null);";
    mysqli_query($conn, $sql);
    $sql = " Create TABLE IF NOT EXISTS cat_prod (
      idCat int not null,
      idProd int not null,
      Foreign key (idCat) references category(id),
      Foreign key (idProd) references product(id),
      unique (idCat, idProd)
    );";
    mysqli_query($conn, $sql);
    echo mysqli_error($conn);
  }
 ?>
