<?php
  function readAllProducts() {
    require_once('dao/mysqli.php');
    $conn = database_connect();

    $sql = "SELECT * from product";
    $list = mysqli_query($conn, $sql);
    mysqli_close($conn);
    return $list;
  }

  function readFromCategory($category) {
    require_once('dao/mysqli.php');
    $conn = database_connect();

    $sql = "SELECT product.name, product.price from product
	   inner join cat_prod on cat_prod.idProd = product.id
	    inner join category on cat_prod.idCat = category.id
	     where category.name = '$category'";
    $list = mysqli_query($conn, $sql);
    mysqli_close($conn);
    return $list;
  }

  function readProduct($product) {
    require_once('dao/mysqli.php');
    $conn = database_connect();

    $sql = "SELECT product.name, product.price, category.name as nameCat
        from product
        inner join cat_prod on cat_prod.idProd = product.id
        inner join category on cat_prod.idCat = category.id
        where product.name = '$product'";

  $list = mysqli_query($conn, $sql);
    mysqli_close($conn);
    return $list;
  }
 ?>
