<!DOCTYPE php>
<?php
include("header.php");
?>
  <form method="POST">
    <fieldset>
      <input type="text" name="login" placeholder="Username"/><br/>
      <input type="password" name="passwd" placeholder="Password"/><br/>
      <input type="submit" value="Log in"/>
    </fieldset>
  </form>
</body>
</html>
<?php
session_start();
require_once("dao/userDAO.php");
if (auth($_POST["login"], $_POST["passwd"]))
{
  $_SESSION["current_user"] = $_POST["login"];
  echo $_SESSION["current_user"];
  header('Location: index.php');
}
else
{
  $_SESSION["current_user"] = "";
  echo "Wrong username or password.";
}
?>
