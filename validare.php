<?php
session_start();
if ($_SESSION["current_user"] == "")
{
  header("Location: login.php");
}
else
{
  $_SESSION["cart"] = array();
  header("Location: index.php");
}
?>
