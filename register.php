<!DOCTYPE php>
<html>
<?php
include('header.php');
?>
  <div class="form-content">
    <form method="POST">
      <fieldset>
        <legend>Create account</legend>
        <input type="text" name="login" placeholder="Username"/> <br/>
        <input type="password" name="passwd" placeholder="Password"/> <br/>
        <input type="password" name="passwdcheck" placeholder="Retype password"/> <br/>
        <input type="submit" value="Register"/>
      </fieldset>
    </form>
  </div>
  <?php
  require_once("dao/userDAO.php");
  if ($_POST["login"] != "" && $_POST["passwd"] != "" && $_POST["passwd"] == $_POST["passwdcheck"])
  {
    echo createUser($_POST["login"], hash('md5', $_POST["passwd"]));
  }
  else
  {
    echo "Invalid input";
  }
  ?>
</body>
</html>
