<!DOCTYPE php>
<head>
  <link rel="stylesheet" type="text/css" href="static/header.css"/>
  <link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet">
</head>
<body>
  <header>
    <div class="nav-menu">
      <div class="login-status">
        <?php
        session_start();
        if ($_SESSION["current_user"] == "")
        {
          echo '<a href="login.php">Log in</a>';
        }
        else
        {
          echo $_SESSION["current_user"];
          echo '<form method="POST" action="logout.php"><input type="submit" value="Logout" /></form>';
        }
        ?>
      </div>
    <table class="menu">
      <tr>
        <td><a href="index.php">Acasă</a></td>
        <td><a href="produse.php">Produse</a></td>
        <td><a href="register.php">Crează cont</a></td>
        <td class="cart"><div><a href="cart.php"><i class="fas fa-shopping-cart"></i></a></div></td>
      </tr>
    </table>
  </div>
  </header>
</body>
